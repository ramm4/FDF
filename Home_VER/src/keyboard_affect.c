/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   keyboard_affect.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lmatvien <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/07/11 18:00:00 by lmatvien          #+#    #+#             */
/*   Updated: 2018/07/13 02:51:07 by lmatvien         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fdf_header.h"

static int	pushed_key_3(int key, t_imgr *pack)
{

	int min;
	int max;

	if (pack->box->z_v < 0 && pack->box->z_min == 0)
		max = pack->box->max_y  + (pack->box->z_max * 3) * pack->box->zoom;
	else
		max = pack->box->max_y  + (pack->box->z_min * 3) * pack->box->zoom;
	if (pack->box->z_v > 0)
		min = pack->box->min_y - (pack->box->z_max * 3) * pack->box->zoom;
	else
		min = pack->box->min_y + (pack->box->z_max * 3) * pack->box->zoom;
	if (key == 82)
	{
		pack->box->move_x = 1;
		pack->box->move_y = 1;
	//	pack->box->angle_y = 1;
		pack->box->angle_x = 1;
		pack->box->z_v = 1;
		pack->box->zoom = 1;
		pack->box->gradient = 0;
		render(pack);
	}
	if (key == 69 && min > 10)
	{
		pack->box->z_v += 3;
		render(pack);
	}
	if (key == 78 && pack->sz[1] - max > 10)
	{
		pack->box->z_v -= 3;
		render(pack);
	}
	return (1);
}

static int	pushed_key_2(int key, t_imgr *pack)
{
	int min;
	int max;

	if (key == 126 && pack->box->min_y - 5 > 5)
	{
		pack->box->move_y -= 5;
		render(pack);
	}
	else if (key == 75 || key == 67)
		keyboard_zooming(key, pack);
	else if (key == 71)
	{
		if (!pack->box->gradient)
		{
			ft_printf("sdsdsd\n");
			pack->box->gradient = 1;
		}
		else
			pack->box->gradient = 0;
			render(pack);
	}
	else
		pushed_key_3(key, pack);
	return (1);
}

int			pushed_key(int key, t_imgr *pack)
{
	if (key == 53)
	{
		ft_printf("{red}{bold}{unline}Exit. See you soon!{eoc}\n");
		lst_cleaner(&pack->box->p_begin);
		exit(0);
	}
	if (key == 124)
	{

		pack->box->move_x += 5;
		render(pack);
	}
	if (key == 123)
	{
		pack->box->move_x -= 5;
		render(pack);
	}
	if (key == 125 && pack->box->max_y + 5 < pack->sz[1] - 10)
	{
		pack->box->move_y += 5;
		render(pack);
	}
	else
		pushed_key_2(key, pack);
	return (1);
}
