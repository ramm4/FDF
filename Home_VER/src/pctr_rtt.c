/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   pctr_rtt.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lmatvien <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/07/17 12:57:34 by lmatvien          #+#    #+#             */
/*   Updated: 2018/07/17 12:57:35 by lmatvien         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fdf_header.h"

#define X_MOD tk->x_mod
#define Y_MOD tk->y_mod

static void flatness_x(t_p_fillit *lst, t_pixel_pack *tk)
{
	int x_cent;
	int y_cent;
	int x_temp;
	int y_temp;

	x_cent = lst->min_x;
	y_cent = lst->max_y;
	ft_printf("%i    %i\n", x_cent, y_cent);
	x_temp = x_cent + (X_MOD - x_cent) * cos(lst->swerve) - (Y_MOD - y_cent) * sin(lst->swerve);
	y_temp = y_cent + (Y_MOD - y_cent) * cos(lst->swerve) + (X_MOD - x_cent) * sin(lst->swerve);
	X_MOD = x_temp;
	Y_MOD = y_temp; 
}


void check_rotate(t_p_fillit *lst)
{
	if (lst->swerve > 0.0)
	{
		lst->CUR_POS = lst->BEG_POS;
		while (lst->CUR_POS)
		{
			flatness_x(lst, lst->CUR_POS);
			lst->CUR_POS = lst->CUR_POS->n_node;
		}
	}
}