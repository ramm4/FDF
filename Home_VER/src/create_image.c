/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   create_image.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lmatvien <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/07/10 10:32:58 by lmatvien          #+#    #+#             */
/*   Updated: 2018/07/13 02:59:54 by lmatvien         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fdf_header.h"

#define M_T mlx_get_data_addr

static void	wnd_sz(t_p_fillit *pack, int *sz)
{
	if (pack->width <= 20)
		sz[0] = 760;
	else if (pack->width >= 100)
		sz[0] = 1400 + (pack->width * 0.75);
	else
		sz[0] = 900 + (pack->width * 0.75);
	if (pack->rows <= 20)
		sz[1] = 760;
	else if (pack->width >= 100)
		sz[1] = 960 + (pack->width * 0.75);
	else
		sz[1] = 700 + (pack->width * 0.75);
}

void		create_window(t_p_fillit *pack)
{
	t_image_pack	wnd;
	t_imgr			box;
	int				sz[2];

	wnd_sz(pack, &(sz[0]));
	wnd.p_mlx_win = mlx_init();
	sz[0] -= 10;
	sz[1] -= 10;
	wnd.p_win = mlx_new_window(wnd.p_mlx_win, sz[0], sz[1], "Spatial mapping");
	wnd.p_img = mlx_new_image(wnd.p_mlx_win, sz[0], sz[1]);
	wnd.p_table = M_T(wnd.p_img, &(wnd.bdd), &(wnd.size), &(wnd.endian));
	zoom_setting(wnd.p_table, pack, &sz[0]);
	mlx_put_image_to_window(wnd.p_mlx_win, wnd.p_win, wnd.p_img, 10, 10);
	box.sz = &sz[0];
	box.tb = &wnd;
	box.box = pack;
	mlx_hook(wnd.p_win, 17, (1L << 17), &destroy_window, pack->p_begin);
	mlx_hook(wnd.p_win, 2, (1L << 0), &pushed_key, &box);
	mlx_loop(wnd.p_mlx_win);
}
