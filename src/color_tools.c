/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   color_tools.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lmatvien <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/07/13 03:05:37 by lmatvien          #+#    #+#             */
/*   Updated: 2018/07/13 03:11:33 by lmatvien         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fdf_header.h"

#define M_T mlx_get_data_addr

void	filling_color(int *color_bite, t_pixel_pack *block)
{
	color_bite[0] = block->rgb_color;
}

void	render(t_imgr *pack)
{
	mlx_destroy_image(pack->tb->p_mlx_win, pack->tb->p_img);
	pack->tb->p_img = mlx_new_image(pack->tb->p_mlx_win,
									pack->sz[0], pack->sz[1]);
	pack->tb->p_table = M_T(pack->tb->p_img, &(pack->tb->bdd),
							&(pack->tb->size), &(pack->tb->endian));
	zoom_setting(pack->tb->p_table, pack->box, &pack->sz[0]);
	mlx_put_image_to_window(pack->tb->p_mlx_win, pack->tb->p_win,
							pack->tb->p_img, 10, 10);
}