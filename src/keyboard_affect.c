/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   keyboard_affect.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lmatvien <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/07/11 18:00:00 by lmatvien          #+#    #+#             */
/*   Updated: 2018/07/13 02:51:07 by lmatvien         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fdf_header.h"


static int	pushed_key_4(int key, t_imgr *pack)
{
	if (key == 82)
	{
		pack->box->move_x = 0;
		pack->box->move_y = 0;
		pack->box->angle_y = 0;
		pack->box->angle_x = 0;
		pack->box->z_v = 1;
		render(pack);
	}
	if (key == 116 && pack->box->min_y - (int)(pack->box->z_max * 3) > 5)
	{
		ft_printf("%i\n", pack->box->min_y - (int)(pack->box->z_max * 3));
		pack->box->z_v += 3;
		render(pack);
	}
	if (key == 121 && pack->box->max_y + (int)(pack->box->z_max * 3) < pack->sz[1] - 10)
	{
		ft_printf("%i\n", pack->box->min_y - (int)(pack->box->z_max * 3));
		pack->box->z_v -= 3;
		render(pack);
	}
	return (1);
}

static int	pushed_key_3(int key, t_imgr *pack)
{
	int key78;

	key78 = (pack->box->angle_y - 1 * pack->box->width);
	if (key == 69 && pack->box->max_y +
		(pack->box->angle_y + 1 * pack->box->width) * pack->box->zoom < pack->sz[1] - 10)
	{
		pack->box->angle_y += 1;
		render(pack);
	}
	if (key == 78 && (pack->box->min_y - FT_ABS(key78)) > 5)
	{
		pack->box->angle_y -= 1;
		render(pack);
	}
	else
		pushed_key_4(key, pack);
	return (1);
}

static int	pushed_key_2(int key, t_imgr *pack)
{
	if (key == 126 && pack->box->min_y - 5 > 5)
	{
		pack->box->move_y -= 5;
		render(pack);
	}
	if (key == 75)
	{
		pack->box->angle_x += 1;
		render(pack);
	}
	if (key == 67)
	{
		pack->box->angle_x -= 1;
		render(pack);
	}
	else
		pushed_key_3(key, pack);
	return (1);
}

int			pushed_key(int key, t_imgr *pack)
{
	if (key == 53)
	{
		ft_printf("{red}{bold}{unline}Exit. See you soon!{eoc}\n");
		lst_cleaner(&pack->box->p_begin);
		exit(0);
	}
	if (key == 124)
	{
		pack->box->move_x += 5;
		render(pack);
	}
	if (key == 123)
	{
		pack->box->move_x -= 5;
		render(pack);
	}
	if (key == 125 && pack->box->max_y + 5 < pack->sz[1] - 10)
	{
		pack->box->move_y += 5;
		render(pack);
	}
	else
		pushed_key_2(key, pack);
	return (1);
}
